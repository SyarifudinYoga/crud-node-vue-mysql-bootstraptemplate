import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Dashboard'
import Login from '../Login.vue'
import Registrasi from '../Registrasi.vue'
import Pendidikan from '@/views/PendidikanList'
import TambahPendidikan from '@/views/AddPendidikan'
import EditPendidikan from '@/views/EditPendidikan'
import Pekerjaan from '@/views/PekerjaanList'
import TambahPekerjaan from '@/views/AddPekerjaan'
import EditPekerjaan from '@/views/EditPekerjaan'
import Siswa from '@/views/SiswaList'
import TambahSiswa from '@/views/AddSiswa'
import EditSiswa from '@/views/EditSiswa'
import Ayah from '@/views/AyahList'
import TambahAyah from '@/views/AddAyah'
import EditAyah from '@/views/EditAyah'
import Ibu from '@/views/IbuList'
import TambahIbu from '@/views/AddIbu'
import EditIbu from '@/views/EditIbu'
import Wali from '@/views/WaliList'
import TambahWali from '@/views/AddWali'
import EditWali from '@/views/EditWali'
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: '/',
            component: Login,
        },
        {
            path: '/halamanawal',
            name: 'halamanawal',
            component: Login
        },
        {
            path: '/registrasi',
            name: 'registrasi',
            component: Registrasi
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Home
        },
        {
            path: '/pendidikan',
            name: 'PendidikanList',
            component: Pendidikan
        },
        {
            path: '/pendidikan/add',
            name: 'TambahPendidikan',
            component: TambahPendidikan
        },
        {
            path: '/pendidikan/update/:id',
            name: 'EditPendidikan',
            component: EditPendidikan
        },
        {
            path: '/pekerjaan',
            name: 'PekerjaanList',
            component: Pekerjaan
        },
        {
            path: '/pekerjaan/add',
            name: 'TambahPekerjaan',
            component: TambahPekerjaan
        },
        {
            path: '/pekerjaan/update/:id',
            name: 'EditPekerjaan',
            component: EditPekerjaan
        },
        {
            path: '/siswa',
            name: 'SiswaList',
            component: Siswa
        },
        {
            path: '/siswa/add',
            name: 'TambahSiswa',
            component: TambahSiswa
        },
        {
            path: '/siswa/update/:id',
            name: 'EditSiswa',
            component: EditSiswa
        },
        {
            path: '/ayah',
            name: 'AyahList',
            component: Ayah
        },
        {
            path: '/ayah/add',
            name: 'TambahAyah',
            component: TambahAyah
        },
        {
            path: '/ayah/update/:id',
            name: 'EditAyah',
            component: EditAyah
        },
        {
            path: '/ibu',
            name: 'IbuList',
            component: Ibu
        },
        {
            path: '/ibu/add',
            name: 'TambahIbu',
            component: TambahIbu
        },
        {
            path: '/ibu/update/:id',
            name: 'EditIbu',
            component: EditIbu
        },
        {
            path: '/wali',
            name: 'WaliList',
            component: Wali
        },
        {
            path: '/wali/add',
            name: 'TambahWali',
            component: TambahWali
        },
        {
            path: '/wali/update/:id',
            name: 'EditWali',
            component: EditWali
        }
    ]
});