import Ayah from "../../../src/DummyTesting/Ayah/AyahList.vue"
import axios from 'axios';
import { shallowMount } from '@vue/test-utils';

// reset semua pemalsuan (mock) setiap selesai test
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('memanggil Rest API tabel ayah dengan Axios', async (done) => {
    const mockSuccessResponse = [
            {
                "kode_ayah": "-",
                "nama_ayah": "-",
                "nik_ayah": "-",
                "tahun_lahir_ayah": 0,
                "kode_pendidikan": "01",
                "kode_pekerjaan": "01",
                "kode_penghasilan": "1",
                "kode_kebutuhan_khusus": "01"
            },
            {
                "kode_ayah": "AY0001",
                "nama_ayah": "Budi",
                "nik_ayah": "3210087687676528",
                "tahun_lahir_ayah": 1987,
                "kode_pendidikan": "04",
                "kode_pekerjaan": "04",
                "kode_penghasilan": "2",
                "kode_kebutuhan_khusus": "01"
            },
            {
                "kode_ayah": "AY0002",
                "nama_ayah": "Munandar",
                "nik_ayah": "3210087687676382",
                "tahun_lahir_ayah": 1977,
                "kode_pendidikan": "06",
                "kode_pekerjaan": "02",
                "kode_penghasilan": "1",
                "kode_kebutuhan_khusus": "01"
            },
            {
                "kode_ayah": "AY0003",
                "nama_ayah": "Rafi Jaelani",
                "nik_ayah": "3210087687676222",
                "tahun_lahir_ayah": 1969,
                "kode_pendidikan": "10",
                "kode_pekerjaan": "05",
                "kode_penghasilan": "5",
                "kode_kebutuhan_khusus": "01"
            },
            {
                "kode_ayah": "AY0004",
                "nama_ayah": "Arbani",
                "nik_ayah": "321008768767529",
                "tahun_lahir_ayah": 1987,
                "kode_pendidikan": "01",
                "kode_pekerjaan": "01",
                "kode_penghasilan": "1",
                "kode_kebutuhan_khusus": "01"
            }    
    ];
    const mockFetchPromise = Promise.resolve({
      data: mockSuccessResponse,
    });
    // memalsukan fungsi fetch API
    // selalu mengembalikan nilai sesuai yang diinginkan
    axios.get = jest.fn().mockResolvedValue(mockFetchPromise);
    const wrapper = shallowMount(Ayah);
    await wrapper.vm.fetchDataAxios();
    // memastikan fungsi fetch dipanggil sekali
    
    // memastikan fungsi dipanggil dengan URL yang benar
    expect(axios.get).toHaveBeenCalledWith('http://localhost:5000/ayah/view');
    expect(wrapper.vm.dataResponse).toEqual(mockSuccessResponse);
    done();
  });