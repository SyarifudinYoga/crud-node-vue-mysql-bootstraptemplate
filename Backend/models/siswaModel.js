// import connection
import db from "../config/database.js";

// Get All Products
export const getSiswa = (result) => {
    db.query("SELECT D.nik, D.nama_lengkap, D.jenis_kelamin, concat(D.tempat_lahir,',', Date_format(D.tanggal_lahir,'%d-%m-%Y')) as tempat_tanggal_lahir, O.kode_ayah, O.nama_ayah, A.kode_Alamat, D.alamat_jalan FROM tb_data_diri D INNER JOIN tb_alamat A ON A.kode_Alamat = D.kode_Alamat INNER JOIN tb_ayah O ON O.kode_ayah = D.kode_ayah", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const getSiswaById = (id, result) => {
    db.query(`SELECT nik, nama_lengkap, jenis_kelamin, nisn, no_kitas, tempat_lahir, 
            DATE_FORMAT(tanggal_lahir, '%Y-%m-%d') AS tanggal_lahir, agama,kewarganegaraan, nama_negara, berkebutuhan_khusus,
            kode_alamat, alamat_jalan, rt, rw, tempat_tinggal, mode_transportasi, no_kps, no_kip, bank, nomor_regis_akta_lahir,
            no_rek_bank, nama_rekening, kode_ayah, kode_ibu, kode_wali, tinggi_badan, berat_badan, jarak_ke_sekolah, sebutkan_jarak, waktu_tempuh_jam,
            waktu_tempuh_menit, jumlah_saudara_kandung, kode_prestasi, kode_beasiswa, kode_regis, nomor_peserta_ujian, kode_keluar
            FROM tb_data_diri WHERE nik = ?`, [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

// Insert Product to Database
export const insertSiswa = (data, result) => {
    db.query("INSERT INTO tb_data_diri SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update Product to Database
export const updateSiswaById = (data, id, result) => {
    db.query("UPDATE tb_data_diri SET nama_lengkap=?, jenis_kelamin=?, nisn=?, no_kitas=?, tempat_lahir=?, tanggal_lahir=?, agama=?, kewarganegaraan=?, nama_negara=?, berkebutuhan_khusus=?, kode_alamat=?, alamat_jalan=?, rt=?, rw=?, tempat_tinggal=?, mode_transportasi=?, no_kps=?, no_kip=?, bank=?, nomor_regis_akta_lahir=?, no_rek_bank=?, nama_rekening=?, kode_ayah=?, kode_ibu=?, kode_wali=?, tinggi_badan=?, berat_badan=?, jarak_ke_sekolah=?, sebutkan_jarak=?, waktu_tempuh_jam=?, waktu_tempuh_menit=?, jumlah_saudara_kandung=?, kode_prestasi=?, kode_beasiswa=?, kode_regis=?, nomor_peserta_ujian=?, kode_keluar=? WHERE nik=?", 
    [data.nama_lengkap, data.jenis_kelamin, data.nisn, data.no_kitas, data.tempat_lahir, data.tanggal_lahir, data.agama, data.kewarganegaraan, data.nama_negara, 
    data.berkebutuhan_khusus, data.kode_alamat, data.alamat_jalan, data.rt, data.rw, data.tempat_tinggal, data.mode_transportasi, data.no_kps, data.no_kip, data.bank,
    data.nomor_regis_akta_lahir, data.no_rek_bank, data.nama_rekening, data.kode_ayah, data.kode_ibu, data.kode_wali, data.tinggi_badan, data.berat_badan,data.jarak_ke_sekolah,
    data.sebutkan_jarak, data.waktu_tempuh_jam, data.waktu_tempuh_menit, data.jumlah_saudara_kandung, data.kode_prestasi, data.kode_beasiswa, data.kode_regis,
    data.nomor_peserta_ujian, data.kode_keluar,id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Delete Product to Database
export const deleteSiswaById = (id, result) => {
    db.query("DELETE FROM tb_data_diri WHERE nik = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const searchSiswa = (id, result) => {
    db.query("SELECT D.nik, D.nama_lengkap, D.jenis_kelamin, concat(D.tempat_lahir,',', Date_format(D.tanggal_lahir,'%d-%m-%Y')) as tempat_tanggal_lahir, O.kode_ayah, O.nama_ayah, A.kode_Alamat, D.alamat_jalan FROM tb_data_diri D INNER JOIN tb_alamat A ON A.kode_Alamat = D.kode_Alamat INNER JOIN tb_ayah O ON O.kode_ayah = D.kode_ayah WHERE D.nik LIKE '%"+[id]+"%' OR D.nama_lengkap LIKE '%"+[id]+"%'", (err, results) => {             
    if(err) {
        console.log(err);
        result(err, null);
    } else {
        result(null, results);
    }
});
}