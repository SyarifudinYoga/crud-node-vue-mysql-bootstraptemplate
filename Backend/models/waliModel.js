// import connection
import db from "../config/database.js";

// Get All Products
export const getWali = (result) => {
    db.query("SELECT * FROM tbl_wali", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const getWaliById = (id, result) => {
    db.query("SELECT * FROM tbl_wali WHERE kode_wali = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

// Insert Product to Database
export const insertWali = (data, result) => {
    db.query("INSERT INTO tbl_wali SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update Product to Database
export const updateWaliById = (data, id, result) => {
    db.query("UPDATE tbl_wali SET nama_wali = ?,nik_wali=?,tahun_lahir_wali=?,kode_pendidikan=?,kode_pekerjaan=?,kode_penghasilan=? WHERE kode_wali = ?", 
    [data.nama_wali, data.nik_wali, data.tahun_lahir_wali, data.kode_pendidikan, data.kode_pekerjaan, data.kode_penghasilan, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Delete Product to Database
export const deleteWaliById = (id, result) => {
    db.query("DELETE FROM tbl_wali WHERE kode_wali = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const searchWali = (id, result) => {
    db.query("SELECT * FROM tbl_wali WHERE nama_wali LIKE '%"+[id]+"%' OR nik_wali LIKE '%"+[id]+"%'", (err, results) => {             
    if(err) {
        console.log(err);
        result(err, null);
    } else {
        result(null, results);
    }
});
}