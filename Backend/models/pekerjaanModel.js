// import connection
import db from "../config/database.js";

// Get All Products
export const getPekerjaan = (result) => {
    db.query("SELECT * FROM tb_pekerjaan", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const getPekerjaanById = (id, result) => {
    db.query("SELECT * FROM tb_pekerjaan WHERE kode_pekerjaan = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

// Insert Product to Database
export const insertPekerjaan = (data, result) => {
    db.query("INSERT INTO tb_pekerjaan SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update Product to Database
export const updatePekerjaanById = (data, id, result) => {
    db.query("UPDATE tb_pekerjaan SET nama_pekerjaan = ? WHERE kode_pekerjaan = ?", [data.nama_pekerjaan, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Delete Product to Database
export const deletePekerjaanById = (id, result) => {
    db.query("DELETE FROM tb_pekerjaan WHERE kode_pekerjaan = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const searchPekerjaan = (id, result) => {
    db.query("SELECT * FROM tb_pekerjaan WHERE nama_pekerjaan LIKE '%"+[id]+"%'", (err, results) => {             
    if(err) {
        console.log(err);
        result(err, null);
    } else {
        result(null, results);
    }
});
}