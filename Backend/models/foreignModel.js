// import connection
import db from "../config/database.js";

// Get Data Alamat
export const getAlamat = (result) => {
    db.query("SELECT * FROM tb_alamat", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
// Get Data KPS
export const getKPS = (result) => {
    db.query("SELECT * FROM tb_kps", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
// Get Data KIP
export const getKIP = (result) => {
    db.query("SELECT * FROM tb_kip", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
// Get Data Ayah
export const getAyah = (result) => {
    db.query("SELECT * FROM tb_ayah", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Ibu
export const getIbu = (result) => {
    db.query("SELECT * FROM tb_ibu", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Wali
export const getWali = (result) => {
    db.query("SELECT * FROM tbl_wali", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Prestasi
export const getPrestasi = (result) => {
    db.query("SELECT * FROM tb_prestasi", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Prestasi
export const getBeasiswa = (result) => {
    db.query("SELECT * FROM tb_beasiswa", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Prestasi
export const getRegis = (result) => {
    db.query("SELECT * FROM tb_regis", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Prestasi
export const getUjian = (result) => {
    db.query("SELECT * FROM tb_ujian", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Prestasi
export const getKeluar = (result) => {
    db.query("SELECT * FROM tb_keluar", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Penghasilan
export const getPenghasilan = (result) => {
    db.query("SELECT * FROM tbl_penghasilan", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Get Data Kebutuhan Khusus
export const getKebutuhanKhusus = (result) => {
    db.query("SELECT * FROM tb_kebutuhan_khusus", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
//Dashboard
export const getUsiaSiswa = (result) => {
    db.query(`SELECT CASE WHEN
            umur > 7 AND jenis_kelamin = 'Laki-laki' THEN 'Laki-Laki > 7 Tahun' WHEN umur > 7 AND jenis_kelamin = 'Perempuan' THEN 'Perempuan > 7 Tahun' WHEN umur < 6 AND jenis_kelamin = 'Laki-laki' THEN 'Laki-Laki < 6 Tahun' WHEN umur < 6 AND jenis_kelamin = 'Perempuan' THEN 'Perempuan < 6 Tahun' WHEN umur BETWEEN 6 AND 7 AND jenis_kelamin = 'Laki-laki' THEN 'Laki-Laki 6-7 Tahun' WHEN umur BETWEEN 6 AND 7 AND jenis_kelamin = 'Perempuan' THEN 'Perempuan 6-7 Tahun' WHEN umur IS NULL THEN '(null)'
            END AS usia,
            COUNT(*) AS jumlah
            FROM
                (SELECT jenis_kelamin,TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur FROM tb_data_diri) AS hasil_usia GROUP BY usia`, (err, results) => {
                if(err) {
                    console.log(err);
                    result(err, null);
                } else {
                    result(null, results);
                }
            }); 
}