// import connection
import db from "../config/database.js";

// Get All Products
export const getIbu = (result) => {
    db.query("SELECT * FROM tb_ibu", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const getIbuById = (id, result) => {
    db.query("SELECT * FROM tb_ibu WHERE kode_ibu = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

// Insert Product to Database
export const insertIbu = (data, result) => {
    db.query("INSERT INTO tb_ibu SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update Product to Database
export const updateIbuById = (data, id, result) => {
    db.query("UPDATE tb_ibu SET nama_ibu = ?,nik_ibu=?, tahun_lahir_ibu=?,kode_pendidikan=?,kode_pekerjaan=?,kode_penghasilan=?,kode_kebutuhan_khusus=? WHERE kode_ibu = ?", 
    [data.nama_ibu, data.nik_ibu, data.tahun_lahir_ibu, data.kode_pendidikan, data.kode_pekerjaan, data.kode_penghasilan, data.kode_kebutuhan_khusus, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Delete Product to Database
export const deleteIbuById = (id, result) => {
    db.query("DELETE FROM tb_ibu WHERE kode_ibu = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const searchIbu = (id, result) => {
    db.query("SELECT * FROM tb_ibu WHERE nama_ibu LIKE '%"+[id]+"%' OR nik_ibu LIKE '%"+[id]+"%'", (err, results) => {             
    if(err) {
        console.log(err);
        result(err, null);
    } else {
        result(null, results);
    }
});
}