// import connection
import db from "../config/database.js";

// Get All Products
export const getAyah = (result) => {
    db.query("SELECT * FROM tb_ayah", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const getAyahById = (id, result) => {
    db.query("SELECT * FROM tb_ayah WHERE kode_ayah = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

// Insert Product to Database
export const insertAyah = (data, result) => {
    db.query("INSERT INTO tb_ayah SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update Product to Database
export const updateAyahById = (data, id, result) => {
    db.query("UPDATE tb_ayah SET nama_ayah = ?,nik_ayah=?,tahun_lahir_ayah=?,kode_pendidikan=?,kode_pekerjaan=?,kode_penghasilan=?,kode_kebutuhan_khusus=? WHERE kode_ayah = ?", 
    [data.nama_ayah, data.nik_ayah, data.tahun_lahir_ayah, data.kode_pendidikan, data.kode_pekerjaan, data.kode_penghasilan, data.kode_kebutuhan_khusus, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Delete Product to Database
export const deleteAyahById = (id, result) => {
    db.query("DELETE FROM tb_ayah WHERE kode_ayah = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const searchAyah = (id, result) => {
        db.query("SELECT * FROM tb_ayah WHERE nama_ayah LIKE '%"+[id]+"%' OR nik_ayah LIKE '%"+[id]+"%'", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });
}