const axios = require('axios');

class Ibu {

     static async getIbu() {
        let res = await axios.get('http://localhost:5000/ibu/view');
        return res;
    }

    static async getIbuById() {
        var id="IB001";
        let res = await axios.get('http://localhost:5000/ibu/view/'+id);
        return res;
    }

    static async getIbuByNama() {
        var searchNamaIbu = "San";
        let res = await axios.get('http://localhost:5000/ibu/search/'+searchNamaIbu);
        return res;
    }

    static async getIbuByNik() {
        var searchNikIbu = "34534253628";
        let res = await axios.get('http://localhost:5000/ibu/search/'+searchNikIbu);
        return res;
    }

    static async getIbuWithoutParams() {
        let res = await axios.get('http://localhost:5000/ibu/search/');
        return res;
    }

    static async postIbu() {
        var kode_ibu= "IB005";
        var nama_ibu= "Test Ibu 2";
        var nik_ibu= "3453425362800";
        var tahun_lahir_ibu= "1979";
        var kode_pendidikan= "01";
        var kode_pekerjaan= "01";
        var kode_penghasilan= "1";
        var kode_kebutuhan_khusus= "01";
        let res = await axios.post('http://localhost:5000/ibu/add/',{
            kode_ibu,
            nama_ibu,
            nik_ibu,
            tahun_lahir_ibu,
            kode_pendidikan,
            kode_pekerjaan,
            kode_penghasilan,
            kode_kebutuhan_khusus
        });
        return res;
    }

    static async putIbu() {
        var kode_ibu= "IB004";
        var nama_ibu= "Test Ibu";
        var nik_ibu= "3453425362811";
        var tahun_lahir_ibu= "1980";
        var kode_pendidikan= "01";
        var kode_pekerjaan= "01";
        var kode_penghasilan= "1";
        var kode_kebutuhan_khusus= "01";
        let res = await axios.put('http://localhost:5000/ibu/update/'+kode_ibu,{
            kode_ibu,
            nama_ibu,
            nik_ibu,
            tahun_lahir_ibu,
            kode_pendidikan,
            kode_pekerjaan,
            kode_penghasilan,
            kode_kebutuhan_khusus
        });
        return res;
    }

    static async deleteIbu() {
        var kode_ibu= "IB005";
        let res = await axios.delete('http://localhost:5000/ibu/delete/'+kode_ibu);
        return res;
    }

}

module.exports = Ibu;