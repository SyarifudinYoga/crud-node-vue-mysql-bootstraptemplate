const axios = require('axios');

class Ayah {

     static async getAyah() {
        let ga = await axios.get('http://localhost:5000/ayah/view');
        return ga;
    }

    static async getAyahById() {
        var id="AY0004";
        let gabi = await axios.get('http://localhost:5000/ayah/view/'+id);
        return gabi;
    }

    static async getAyahByNama() {
        var searchNamaAyah = "Test";
        let gabn = await axios.get('http://localhost:5000/ayah/search/'+searchNamaAyah);
        return gabn;
    }

    static async getAyahByNik() {
        var searchNikAyah = "3424514343";
        let gabnik = await axios.get('http://localhost:5000/ayah/search/'+searchNikAyah);
        return gabnik;
    }

    static async getAyahWithoutParams() {
        let gawp = await axios.get('http://localhost:5000/ayah/search/');
        return gawp;
    }

    static async postAyah() {
        var kode_ayah= "AY0005";
        var nama_ayah= "Test Ayah";
        var nik_ayah= "3424514343234";
        var tahun_lahir_ayah= "1977";
        var kode_pendidikan= "01";
        var kode_pekerjaan= "01";
        var kode_penghasilan= "1";
        var kode_kebutuhan_khusus= "01";
        let posta = await axios.post('http://localhost:5000/ayah/add/',{
            kode_ayah,
            nama_ayah,
            nik_ayah,
            tahun_lahir_ayah,
            kode_pendidikan,
            kode_pekerjaan,
            kode_penghasilan,
            kode_kebutuhan_khusus
        });
        return posta;
    }

    static async putAyah() {
        var kode_ayah= "AY0004";
        var nama_ayah= "Test Update Ayah";
        var nik_ayah= "3424514343234";
        var tahun_lahir_ayah= "1977";
        var kode_pendidikan= "01";
        var kode_pekerjaan= "01";
        var kode_penghasilan= "1";
        var kode_kebutuhan_khusus= "01";
        let puta = await axios.put('http://localhost:5000/ayah/update/'+kode_ayah,{
            kode_ayah,
            nama_ayah,
            nik_ayah,
            tahun_lahir_ayah,
            kode_pendidikan,
            kode_pekerjaan,
            kode_penghasilan,
            kode_kebutuhan_khusus
        });
        return puta;
    }

    static async deleteAyah() {
        var kode_ayah="AY0005";
        let dela = await axios.delete('http://localhost:5000/ayah/delete/'+kode_ayah);
        return dela;
    }

}

module.exports = Ayah;