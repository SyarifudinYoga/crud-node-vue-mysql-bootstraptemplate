const axios = require('axios');

class Siswa {

     static async getSiswa() {
        let res = await axios.get('http://localhost:5000/siswa/view');
        return res;
    }

    static async getSiswaById() {
        var id="328173536472111";
        let res = await axios.get('http://localhost:5000/siswa/view/'+id);
        return res;
    }

    static async getSiswaByNama() {
        var searchNamaSiswa = "Syarifudin";
        let res = await axios.get('http://localhost:5000/siswa/search/'+searchNamaSiswa);
        return res;
    }

    static async getSiswaByNik() {
        var searchNikSiswa = "32817353647211";
        let res = await axios.get('http://localhost:5000/siswa/search/'+searchNikSiswa);
        return res;
    }

    static async getSiswaWithoutParams() {
        let res = await axios.get('http://localhost:5000/siswa/search/');
        return res;
    }

    static async postSiswa() {
        var nik= "11111";
        var nama_lengkap= "Test Siswa 2";
        var jenis_kelamin= "Perempuan";
        var nisn= "123456789";
        var no_kitas= "0";
        var tempat_lahir= "Bandung";
        var tanggal_lahir= "2021-02-01";
        var agama= "02";
        var kewarganegaraan= "Indonesia (WNI)";
        var nama_negara= "Indonesia";
        var berkebutuhan_khusus= "01";
        var kode_alamat= "AL006";
        var alamat_jalan= "jl.Cibeber. Cimahi Selatan No.88";
        var rt= "003";
        var rw= "002";
        var tempat_tinggal= "1";
        var mode_transportasi= "02";
        var no_kps= "123";
        var no_kip= "0878651";
        var bank= "Mandiri";
        var nomor_regis_akta_lahir= "0000";
        var no_rek_bank= "0987656";
        var nama_rekening= "Udin";
        var kode_ayah= "AY0001";
        var kode_ibu= "IB001";
        var kode_wali= "W001";
        var tinggi_badan= "141";
        var berat_badan= "50";
        var jarak_ke_sekolah= "Lebih dari 1 km";
        var sebutkan_jarak= "2000";
        var waktu_tempuh_jam= "1";
        var waktu_tempuh_menit= "20";
        var jumlah_saudara_kandung= "2";
        var kode_prestasi= "PR003";
        var kode_beasiswa= "BE003";
        var kode_regis= "RG003";
        var nomor_peserta_ujian= "UJ002";
        var kode_keluar= "KL003";
        let res = await axios.post('http://localhost:5000/siswa/add/',{
            nik,
            nama_lengkap,
            jenis_kelamin,
            nisn,
            no_kitas,
            tempat_lahir,
            tanggal_lahir,
            agama,
            kewarganegaraan,
            nama_negara,
            berkebutuhan_khusus,
            kode_alamat,
            alamat_jalan,
            rt,
            rw,
            tempat_tinggal,
            mode_transportasi,
            no_kps,
            no_kip,
            bank,
            nomor_regis_akta_lahir,
            no_rek_bank,
            nama_rekening,
            kode_ayah,
            kode_ibu,
            kode_wali,
            tinggi_badan,
            berat_badan,
            jarak_ke_sekolah,
            sebutkan_jarak,
            waktu_tempuh_jam,
            waktu_tempuh_menit,
            jumlah_saudara_kandung,
            kode_prestasi,
            kode_beasiswa,
            kode_regis,
            nomor_peserta_ujian,
            kode_keluar
        });
        return res;
    }

    static async putSiswa() {
        var nik= "1111111";
        var nama_lengkap= "Test Siswa 1";
        var jenis_kelamin= "laki-Laki";
        var nisn= "123456789";
        var no_kitas= "0";
        var tempat_lahir= "Bandung";
        var tanggal_lahir= "2021-02-01";
        var agama= "02";
        var kewarganegaraan= "Indonesia (WNI)";
        var nama_negara= "Indonesia";
        var berkebutuhan_khusus= "01";
        var kode_alamat= "AL006";
        var alamat_jalan= "jl.Cibeber. Cimahi Selatan No.88";
        var rt= "003";
        var rw= "002";
        var tempat_tinggal= "1";
        var mode_transportasi= "02";
        var no_kps= "123";
        var no_kip= "0878651";
        var bank= "Mandiri";
        var nomor_regis_akta_lahir= "0000";
        var no_rek_bank= "0987656";
        var nama_rekening= "Udin";
        var kode_ayah= "AY0001";
        var kode_ibu= "IB001";
        var kode_wali= "W001";
        var tinggi_badan= "141";
        var berat_badan= "50";
        var jarak_ke_sekolah= "Lebih dari 1 km";
        var sebutkan_jarak= "2000";
        var waktu_tempuh_jam= "1";
        var waktu_tempuh_menit= "20";
        var jumlah_saudara_kandung= "2";
        var kode_prestasi= "PR003";
        var kode_beasiswa= "BE003";
        var kode_regis= "RG003";
        var nomor_peserta_ujian= "UJ002";
        var kode_keluar= "KL003";
        let res = await axios.put('http://localhost:5000/siswa/update/'+nik,{
            nik,
            nama_lengkap,
            jenis_kelamin,
            nisn,
            no_kitas,
            tempat_lahir,
            tanggal_lahir,
            agama,
            kewarganegaraan,
            nama_negara,
            berkebutuhan_khusus,
            kode_alamat,
            alamat_jalan,
            rt,
            rw,
            tempat_tinggal,
            mode_transportasi,
            no_kps,
            no_kip,
            bank,
            nomor_regis_akta_lahir,
            no_rek_bank,
            nama_rekening,
            kode_ayah,
            kode_ibu,
            kode_wali,
            tinggi_badan,
            berat_badan,
            jarak_ke_sekolah,
            sebutkan_jarak,
            waktu_tempuh_jam,
            waktu_tempuh_menit,
            jumlah_saudara_kandung,
            kode_prestasi,
            kode_beasiswa,
            kode_regis,
            nomor_peserta_ujian,
            kode_keluar
        });
        return res;
    }

    static async deleteSiswa() {
        var nis= "11111";
        let res = await axios.delete('http://localhost:5000/siswa/delete/'+nis);
        return res;
    }

}

module.exports = Siswa;