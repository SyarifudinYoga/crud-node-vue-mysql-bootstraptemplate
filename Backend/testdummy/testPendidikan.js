const axios = require('axios');

class Pendidikan {

     static async getPendidikan() {
        let res = await axios.get('http://localhost:5000/pendidikan/view');
        return res;
    }

    static async getPendidikanById() {
        var id="11";
        let res = await axios.get('http://localhost:5000/pendidikan/view/'+id);
        return res;
    }

    static async getPendidikanByNama() {
        var searchNamaPendidikan = "Sederajat";
        let res = await axios.get('http://localhost:5000/pendidikan/search/'+searchNamaPendidikan);
        return res;
    }

    static async getPendidikanWithoutParams() {
        let res = await axios.get('http://localhost:5000/pendidikan/search/');
        return res;
    }

    static async postPendidikan() {
        var kode_pendidikan= "11";
        var nama_pendidikan= "S3";
        let res = await axios.post('http://localhost:5000/pendidikan/add/',{
            kode_pendidikan,
            nama_pendidikan
        });
        return res;
    }

    static async putPendidikan() {
        var kode_pendidikan= "10";
        var nama_pendidikan= "S2";
        let res = await axios.put('http://localhost:5000/pendidikan/update/'+kode_pendidikan,{
            kode_pendidikan,
            nama_pendidikan
        });
        return res;
    }

    static async deletePekerjaan() {
        var id="11";
        let res = await axios.delete('http://localhost:5000/pendidikan/delete/'+id);
        return res;
    }

}

module.exports = Pendidikan;