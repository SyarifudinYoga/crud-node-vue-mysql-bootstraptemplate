const axios = require('axios');

class Pekerjaan {

     static async getPekerjaan() {
        let res = await axios.get('http://localhost:5000/pekerjaan/view');
        return res;
    }

    static async getPekerjaanById() {
        var id="11";
        let res = await axios.get('http://localhost:5000/pekerjaan/view/'+id);
        return res;
    }

    static async getPekerjaanByNama() {
        var searchNamaPekerjaan = "Pe";
        let res = await axios.get('http://localhost:5000/pekerjaan/search/'+searchNamaPekerjaan);
        return res;
    }

    static async getPekerjaanWithoutParams() {
        let res = await axios.get('http://localhost:5000/pekerjaan/search/');
        return res;
    }

    static async postPekerjaan() {
        var kode_pekerjaan= "11";
        var nama_pekerjaan= "Buruh";
        let res = await axios.post('http://localhost:5000/pekerjaan/add/',{
            kode_pekerjaan,
            nama_pekerjaan
        });
        return res;
    }

    static async putPekerjaan() {
        var kode_pekerjaan= "11";
        var nama_pekerjaan= "Buruh Tani";
        let res = await axios.put('http://localhost:5000/pekerjaan/update/'+kode_pekerjaan,{
            kode_pekerjaan,
            nama_pekerjaan
        });
        return res;
    }

    static async deletePekerjaan() {
        var id="11";
        let res = await axios.delete('http://localhost:5000/pekerjaan/delete/'+id);
        return res;
    }

}

module.exports = Pekerjaan;