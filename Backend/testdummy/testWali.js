const axios = require('axios');

class Wali {

     static async getWali() {
        let gw = await axios.get('http://localhost:5000/wali/view');
        return gw;
    }

    static async getWaliById() {
        var id="W003";
        let gwbi = await axios.get('http://localhost:5000/wali/view/'+id);
        return gwbi;
    }

    static async getWaliByNama() {
        var searchNamaWali = "En";
        let gwbnm = await axios.get('http://localhost:5000/wali/search/'+searchNamaWali);
        return gwbnm;
    }

    static async getWaliByNik() {
        var searchNikWali = "34554654654678";
        let gwbn = await axios.get('http://localhost:5000/wali/search/'+searchNikWali);
        return gwbn;
    }

    static async getWaliWithoutParams() {
        let gwwp = await axios.get('http://localhost:5000/wali/search/');
        return gwwp;
    }

    static async postWali() {
        var kode_wali= "W005";
        var nama_wali= "Test Wali 2";
        var nik_wali= "3455465465467179";
        var tahun_lahir_wali= "1970";
        var kode_pendidikan= "01";
        var kode_pekerjaan= "01";
        var kode_penghasilan= "1";
        let postw = await axios.post('http://localhost:5000/wali/add/',{
            kode_wali,
            nama_wali,
            nik_wali,
            tahun_lahir_wali,
            kode_pendidikan,
            kode_pekerjaan,
            kode_penghasilan
        });
        return postw;
    }

    static async putWali() {
        var kode_wali= "W004";
        var nama_wali= "Test Update Wali";
        var nik_wali= "3453425362000";
        var tahun_lahir_wali= "1977";
        var kode_pendidikan= "01";
        var kode_pekerjaan= "01";
        var kode_penghasilan= "1";
        let putw = await axios.put('http://localhost:5000/wali/update/'+kode_wali,{
            kode_wali,
            nama_wali,
            nik_wali,
            tahun_lahir_wali,
            kode_pendidikan,
            kode_pekerjaan,
            kode_penghasilan
        });
        return putw;
    }

    static async deleteWali() {
        var kode_wali= "W005";
        let delw = await axios.delete('http://localhost:5000/wali/delete/'+kode_wali);
        return delw;
    }

}

module.exports = Wali;