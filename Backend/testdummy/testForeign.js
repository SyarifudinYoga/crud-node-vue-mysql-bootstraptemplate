const axios = require('axios');

class Foreign {

     static async getAlamat() {
        let res = await axios.get('http://localhost:5000/alamat/view');
        return res;
    }

    static async getKPS() {
        let res = await axios.get('http://localhost:5000/kps/view');
        return res;
    }

    static async getKIP() {
        let res = await axios.get('http://localhost:5000/kip/view');
        return res;
    }

    static async getPrestasi() {
        let res = await axios.get('http://localhost:5000/prestasi/view');
        return res;
    }

    static async getBeasiswa() {
        let res = await axios.get('http://localhost:5000/beasiswa/view');
        return res;
    }

    static async getRegis() {
        let res = await axios.get('http://localhost:5000/regis/view');
        return res;
    }

    static async getUjian() {
        let res = await axios.get('http://localhost:5000/ujian/view');
        return res;
    }

    static async getKeluar() {
        let res = await axios.get('http://localhost:5000/keluar/view');
        return res;
    }

    static async getPenghasilan() {
        let res = await axios.get('http://localhost:5000/penghasilan/view');
        return res;
    }
    
    static async getKebutuhanKhusus() {
        let res = await axios.get('http://localhost:5000/kebutuhanKhusus/view');
        return res;
    }
    
    static async getUsia() {
        let res = await axios.get('http://localhost:5000/siswa/usia');
        return res;
    }
    
    
}

module.exports = Foreign;