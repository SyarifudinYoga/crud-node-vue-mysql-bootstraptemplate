//import jwt
import jwt from "jsonwebtoken";

const isAuthorized = (request, result, next) => {
    if (typeof request.headers["token"] == "undefined") {
      return result.status(403).json({
        success: false,
        message: "Silahkan Login terlebih dahulu"
      });
    }
  
    let token = request.headers["token"];
  
    jwt.verify(token, "SuperSecRetKey", (err, decoded) => {
      if (err) {
        return result.status(401).json({
          success: false,
          message: "Silahkan login terlebih dahulu"
        });
      }
    });
  
    next();
  };

export default isAuthorized;