const Siswa = require('../testdummy/testSiswa');

//tampil siswa
describe('GET/siswa/view', () =>{
    it('Membuat fungsi untuk tampil data tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Siswa.getSiswa();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
    });
});

//tampil untuk form edit siswa dengan parameter Id
describe('GET/siswa/view/id', () =>{
    var id="328173536472111";
    it('Membuat fungsi untuk tampil data tb_data_diri berdasarkan id dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Siswa.getSiswaById();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data.nik).toBe(id);
        //console.log(result.data);
    });
});

//search siswa by nama
describe('GET/siswa/search/nama', () =>{
    var searchNamaSiswa = "Syarifudin";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Siswa.getSiswaByNama();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nama_lengkap).toContain(searchNamaSiswa);
        //console.log(result.data);
    });
});

//search siswa by nik
describe('GET/siswa/search/nik', () =>{
    var searchNikSiswa = "32817353647211";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Siswa.getSiswaByNik();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nik).toContain(searchNikSiswa);
        //console.log(result.data);
    });
});

//search siswa tanpa parameter
describe('GET/siswa/search', () =>{
    it('Membuat fungsi untuk tampil data tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Siswa.getSiswaWithoutParams();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tambah Siswa
describe('POST/Siswa/Add', () =>{
    it('Membuat fungsi tambah data ke tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var obj=`{"nik":"11111","nama_lengkap":"Test Siswa 2","jenis_kelamin":"Perempuan","nisn":"123456789","no_kitas":"0","tempat_lahir":"Bandung","tanggal_lahir":"2021-02-01","agama":"02","kewarganegaraan":"Indonesia (WNI)","nama_negara":"Indonesia","berkebutuhan_khusus":"01","kode_alamat":"AL006","alamat_jalan":"jl.Cibeber. Cimahi Selatan No.88","rt":"003","rw":"002","tempat_tinggal":"1","mode_transportasi":"02","no_kps":"123","no_kip":"0878651","bank":"Mandiri","nomor_regis_akta_lahir":"0000","no_rek_bank":"0987656","nama_rekening":"Udin","kode_ayah":"AY0001","kode_ibu":"IB001","kode_wali":"W001","tinggi_badan":"141","berat_badan":"50","jarak_ke_sekolah":"Lebih dari 1 km","sebutkan_jarak":"2000","waktu_tempuh_jam":"1","waktu_tempuh_menit":"20","jumlah_saudara_kandung":"2","kode_prestasi":"PR003","kode_beasiswa":"BE003","kode_regis":"RG003","nomor_peserta_ujian":"UJ002","kode_keluar":"KL003"}`;
        const result = await Siswa.postSiswa();
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//edit siswa
describe('PUT/siswa/update/id', () =>{
    var obj=`{"nik":"1111111","nama_lengkap":"Test Siswa 1","jenis_kelamin":"laki-Laki","nisn":"123456789","no_kitas":"0","tempat_lahir":"Bandung","tanggal_lahir":"2021-02-01","agama":"02","kewarganegaraan":"Indonesia (WNI)","nama_negara":"Indonesia","berkebutuhan_khusus":"01","kode_alamat":"AL006","alamat_jalan":"jl.Cibeber. Cimahi Selatan No.88","rt":"003","rw":"002","tempat_tinggal":"1","mode_transportasi":"02","no_kps":"123","no_kip":"0878651","bank":"Mandiri","nomor_regis_akta_lahir":"0000","no_rek_bank":"0987656","nama_rekening":"Udin","kode_ayah":"AY0001","kode_ibu":"IB001","kode_wali":"W001","tinggi_badan":"141","berat_badan":"50","jarak_ke_sekolah":"Lebih dari 1 km","sebutkan_jarak":"2000","waktu_tempuh_jam":"1","waktu_tempuh_menit":"20","jumlah_saudara_kandung":"2","kode_prestasi":"PR003","kode_beasiswa":"BE003","kode_regis":"RG003","nomor_peserta_ujian":"UJ002","kode_keluar":"KL003"}`;
    it('Membuat fungsi untuk mengubah data yang diinputkan dari tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
    const result = await Siswa.putSiswa();
    //cek koneksi ke api
    expect(result.status).toEqual(200);
    //cek data yang diinputkan untuk edit ke db
    expect(result.config.data).toEqual(obj);
    //console.log(result.config);
});
});

//hapus siswa
describe('DELETE/siswa/id', () =>{
    it('Membuat fungsi untuk menghapus data sesuai id yang dipilih dari tb_siswa dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var nis= "11111";
        const result = await Siswa.deleteSiswa();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek status hapus
        expect(result.statusText).toEqual('OK');
        //console.log(result);
    });
});