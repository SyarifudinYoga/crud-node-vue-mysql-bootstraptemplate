const Pendidikan = require('../testdummy/testPendidikan');

//Lihat Pendidikan
describe('GET/pendidikan/view', () =>{
    it('Membuat fungsi untuk tampil data tb_pendidikan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pendidikan.getPendidikan();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
    });
});

//tambah Pendidikan
describe('POST/pendidiikan/add', () =>{
    it('Membuat fungsi untuk tambah data ke tb_pendidikan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var obj = `{"kode_pendidikan":"11","nama_pendidikan":"S3"}`;
        const result = await Pendidikan.postPendidikan();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//form edit
describe('GET/pendidikan/view/id', () =>{
    var id="11";
    it('Membuat fungsi untuk tampil data tb_pendidikan berdasarkan id dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pendidikan.getPendidikanById();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data.kode_pendidikan).toBe(id);
        //console.log(result.data);
    });
});

//search pendidikan by nama pendidikan
describe('GET/pendidikan/search/id', () =>{
    var searchNamaPendidikan = "Sederajat";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_pendidikan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pendidikan.getPendidikanByNama();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nama_pendidikan).toContain(searchNamaPendidikan);
        //console.log(result.data);
    });
});

//search pendidikan without parameter
describe('GET/pendidikan/search/', () =>{
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_pendidikan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pendidikan.getPendidikanWithoutParams();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//edit pendidikan
describe('PUT/pendidikan/update/id', () =>{
    var obj= `{"kode_pendidikan":"10","nama_pendidikan":"S2"}`;
    it('Membuat fungsi untuk mengubah data yang diinputkan dari tb_pendidikan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pendidikan.putPendidikan();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan untuk edit ke db
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//hapus pendidikan
describe('DELETE/pendidikan/id', () =>{
    it('Membuat fungsi untuk menghapus data sesuai id yang dipilih dari tb_pendidikan  dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pendidikan.deletePekerjaan();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek status hapus
        expect(result.statusText).toEqual('OK');
        //console.log(result);
    });
});



