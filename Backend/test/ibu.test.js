const Ibu = require('../testdummy/testIbu');

//tampil ibu
describe('GET/Ibu/View', () =>{
    it('Membuat fungsi untuk tampil data tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Ibu.getIbu();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
    });
});

//tampil untuk form edit ibu dengan parameter Id
describe('GET/ibu/view/id', () =>{
    var id="IB001";
    it('Membuat fungsi untuk tampil data tb_ibu berdasarkan id dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Ibu.getIbuById();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data.kode_ibu).toBe(id);
        //console.log(result.data);
    });
});

//search ibu by nama
describe('GET/ibu/search/nama', () =>{
    var searchNamaIbu = "San";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Ibu.getIbuByNama();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nama_ibu).toContain(searchNamaIbu);
        //console.log(result.data);
    });
});

//search ibu by nik
describe('GET/ibu/search/nik', () =>{
    var searchNikIbu = "34534253628";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Ibu.getIbuByNik();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nik_ibu).toContain(searchNikIbu);
        //console.log(result.data);
    });
});

//search ibu tanpa parameter
describe('GET/ibu/search', () =>{
    it('Membuat fungsi untuk tampil data tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Ibu.getIbuWithoutParams();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tambah Ibu
describe('POST/Ibu/Add', () =>{
    it('Membuat fungsi tambah data ke tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var obj=`{"kode_ibu":"IB005","nama_ibu":"Test Ibu 2","nik_ibu":"3453425362800","tahun_lahir_ibu":"1979","kode_pendidikan":"01","kode_pekerjaan":"01","kode_penghasilan":"1","kode_kebutuhan_khusus":"01"}`;
        const result = await Ibu.postIbu();
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//edit ibu
describe('PUT/ibu/update/id', () =>{
    var obj= `{"kode_ibu":"IB004","nama_ibu":"Test Ibu","nik_ibu":"3453425362811","tahun_lahir_ibu":"1980","kode_pendidikan":"01","kode_pekerjaan":"01","kode_penghasilan":"1","kode_kebutuhan_khusus":"01"}`;
    it('Membuat fungsi untuk mengubah data yang diinputkan dari tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
    const result = await Ibu.putIbu();
    //cek koneksi ke api
    expect(result.status).toEqual(200);
    //cek data yang diinputkan untuk edit ke db
    expect(result.config.data).toEqual(obj);
    //console.log(result.config);
});
});

//hapus ibu
describe('DELETE/ibu/id', () =>{
    it('Membuat fungsi untuk menghapus data sesuai id yang dipilih dari tb_ibu dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Ibu.deleteIbu();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek status hapus
        expect(result.statusText).toEqual('OK');
        //console.log(result);
    });
});