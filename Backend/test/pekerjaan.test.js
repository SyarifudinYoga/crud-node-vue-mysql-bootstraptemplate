const Pekerjaan = require('../testdummy/testPekerjaan');

//hapus pekerjaan
describe('DELETE/pekerjaan/id', () =>{
    it('Membuat fungsi untuk menghapus data sesuai id yang dipilih dari tb_pekerjaan  dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pekerjaan.deletePekerjaan();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek status hapus
        expect(result.statusText).toEqual('OK');
        //console.log(result);
    });
});

//Lihat Pekerjaan
describe('GET/pekerjaan/view', () =>{
    it('Membuat fungsi untuk tampil data tb_pekerjaan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pekerjaan.getPekerjaan();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
    });
});

//tambah Pekerjaan
describe('POST/pekerjaan/add', () =>{
    it('Membuat fungsi untuk tambah data ke tb_pekerjaan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var obj = `{"kode_pekerjaan":"11","nama_pekerjaan":"Buruh"}`;
        const result = await Pekerjaan.postPekerjaan();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//form edit pekerjaan
describe('GET/pekerjaan/view/id', () =>{
    var id="11";
    it('Membuat fungsi untuk tampil data tb_pekerjaan berdasarkan id dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pekerjaan.getPekerjaanById();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data.kode_pekerjaan).toBe(id);
        //console.log(result.data);
    });
});

//search pekerjaan by nama pekerjaan
describe('GET/pekerjaan/search/id', () =>{
    var searchNamaPekerjaan = "Pe";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_pekerjaan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pekerjaan.getPekerjaanByNama();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nama_pekerjaan).toContain(searchNamaPekerjaan);
        //console.log(result.data);
    });
});

//search pekerjaan without parameter
describe('GET/pekerjaan/search/', () =>{
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_pekerjaan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pekerjaan.getPekerjaanWithoutParams();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//edit pekerjaan
describe('PUT/pekerjaan/update/id', () =>{
    var obj= `{"kode_pekerjaan":"11","nama_pekerjaan":"Buruh Tani"}`;
    it('Membuat fungsi untuk mengubah data yang diinputkan dari tb_pekerjaan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Pekerjaan.putPekerjaan();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan untuk edit ke db
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});