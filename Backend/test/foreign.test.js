const Foreign = require('../testdummy/testForeign');

//tampil data Foreign Key alamat
describe('GET/Alamat/View', () =>{
    it('Membuat fungsi untuk tampil data tb_alamat dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Foreign.getAlamat();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key kps
describe('GET/KPS/View', () =>{
    it('Membuat fungsi untuk tampil data tb_kps dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Foreign.getKPS();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key kip
describe('GET/KIP/View', () =>{
    it('Membuat fungsi untuk tampil data tb_kip dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Foreign.getKIP();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key prestasi
describe('GET/Prestasi/View', () =>{
    it('Membuat fungsi untuk tampil data tb_prestasi dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Foreign.getPrestasi();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key beasiswa
describe('GET/Beasiswa/View', () =>{
    it('Membuat fungsi untuk tampil data tb_beasiswa dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getBeasiswa();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key regis
describe('GET/Regis/View', () =>{
    it('Membuat fungsi untuk tampil data tb_regis dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getRegis();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key ujian
describe('GET/Ujian/View', () =>{
    it('Membuat fungsi untuk tampil data tb_ujian dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getUjian();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key keluar
describe('GET/Keluar/View', () =>{
    it('Membuat fungsi untuk tampil data tb_keluar dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getKeluar();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key penghasilan
describe('GET/Penghasilan/View', () =>{
    it('Membuat fungsi untuk tampil data tb_penghasilan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getPenghasilan();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key kebutuhan khusus
describe('GET/KebutuhanKhusus/View', () =>{
    it('Membuat fungsi untuk tampil data tb_kebutuhan_khusus dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getKebutuhanKhusus();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tampil data Foreign Key usia
describe('GET/Usia/View', () =>{
    it('Membuat fungsi untuk tampil data tb_usia dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Foreign.getUsia();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});