const Login = require('../testdummy/testLogin');

//login
describe('POST/pekerjaan/add', () =>{
    it('Membuat fungsi untuk tambah data ke tb_pekerjaan dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        
        const result = await Login.getLogin();
        //console.log(result);
        //cek match jwt
        expect(result.data).toEqual(result.data);
        //cek status
        expect(result.status).toEqual(200);
    });
});

//regis
describe('POST/login/regis', () =>{
    it('Membuat fungsi untuk tambah data ke tb_user dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var obj=`{"id":"6","nama":"Syarifudin Yoga Pinasty","email":"syarifudinyoga33@gmail.com","password":"yoga","no_telp":"08978987887","alamat":"Cimahi"}`;
        const result = await Login.postRegis();
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//dashboard
describe('GET/Ujian/View', () =>{
    it('Membuat fungsi untuk tampil data tb_data_diri dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Login.getSiswa();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

