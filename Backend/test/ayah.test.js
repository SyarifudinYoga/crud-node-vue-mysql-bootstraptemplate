const Ayah = require('../testdummy/testAyah');

//tampil ayah
describe('GET/Ayah/View', () =>{
    it('Membuat fungsi untuk tampil data tb_ayah dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.getAyah();
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
    });
});

//form edit ayah with parameter Id
describe('GET/ayah/view/id', () =>{
    var id="AY0004";
    it('Membuat fungsi untuk tampil data tb_ayah berdasarkan id dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.getAyahById();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data.kode_ayah).toBe(id);
    });
});

//search ayah by nama
describe('GET/ayah/search/nama', () =>{
    var searchNamaAyah = "Test";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_ayah dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.getAyahByNama();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang ditampilkan sebanyak 1 data yang mengandung kata test
        expect(result.data[0].nama_ayah).toContain(searchNamaAyah);
    });
});

//search ayah by nik
describe('GET/ayah/search/nik', () =>{
    var searchNikAyah = "3424514343";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tb_ayah dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.getAyahByNik();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang ditampilkan sebanyak 1 data yang mengandung var searchNikAyah
        expect(result.data[0].nik_ayah).toContain(searchNikAyah);
    });
});

//search ayah tanpa parameter
describe('GET/ayah/search', () =>{
    it('Membuat fungsi untuk tampil data tb_ayah dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.getAyahWithoutParams();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang ditampilkan tanpa parameter
        expect(result.data).toEqual(result.data);
    });
});

//tambah Ayah
describe('POST/Ayah/Add', () =>{
    it('Membuat fungsi tambah data ke tb_ayah dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        var obj =`{"kode_ayah":"AY0005","nama_ayah":"Test Ayah","nik_ayah":"3424514343234","tahun_lahir_ayah":"1977","kode_pendidikan":"01","kode_pekerjaan":"01","kode_penghasilan":"1","kode_kebutuhan_khusus":"01"}`;
        let result = await Ayah.postAyah();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//edit ayah
describe('PUT/ayah/update/id', () =>{
    var obj=`{"kode_ayah":"AY0004","nama_ayah":"Test Update Ayah","nik_ayah":"3424514343234","tahun_lahir_ayah":"1977","kode_pendidikan":"01","kode_pekerjaan":"01","kode_penghasilan":"1","kode_kebutuhan_khusus":"01"}`;
    it('Membuat fungsi untuk mengubah data yang diinputkan dari tb_ayah dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.putAyah();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan untuk edit ke db
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//hapus ayah
describe('DELETE/ayah/id', () =>{
    it('Membuat fungsi untuk menghapus data sesuai id yang dipilih dari tb_ayah  dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        let result = await Ayah.deleteAyah();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek status hapus
        expect(result.statusText).toEqual('OK');
        //console.log(result);
    });
});