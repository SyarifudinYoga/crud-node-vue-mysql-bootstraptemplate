const { default: axios } = require('axios');
const Wali = require('../testdummy/testWali');

//tampil wali
describe('GET/wali/view', () =>{
    it('Membuat fungsi untuk tampil data tbl_wali dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Wali.getWali();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
    });
});

//tampil untuk form edit wali dengan parameter Id
describe('GET/wali/view/id', () =>{
    var id="W003";
    it('Membuat fungsi untuk tampil data tbl_wali berdasarkan id dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Wali.getWaliById();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data.kode_wali).toBe(id);
        //console.log(result.data);
    });
});

//search wali by nama
describe('GET/wali/search/nama', () =>{
    var searchNamaWali = "En";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tbl_wali dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Wali.getWaliByNama();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nama_wali).toContain(searchNamaWali);
        //console.log(result.data);
    });
});

//search wali by nik
describe('GET/wali/search/nik', () =>{
    var searchNikWali = "34554654654678";
    it('Membuat fungsi untuk menampilkan data yang diinputkan dari tbl_wali dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Wali.getWaliByNik();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //mengecek id yang ditampilkan sesuai atau tidak (test dengan console log)
        expect(result.data[0].nik_wali).toContain(searchNikWali);
        //console.log(result.data);
    });
});

//search wali tanpa parameter
describe('GET/wali/search', () =>{
    it('Membuat fungsi untuk tampil data tbl_wali dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Wali.getWaliWithoutParams();
        //Cek koneksi ke api
        expect(result.status).toEqual(200);
        //membandingkan data yang ditampilkan sesuai atau tidak
        expect(result.data).toEqual(result.data);
        //console.log(result.data);
    });
});

//tambah wali
describe('POST/Wali/Add', () =>{
    it('Membuat fungsi tambah data ke tbl_wali dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        
        var obj=`{"kode_wali":"W005","nama_wali":"Test Wali 2","nik_wali":"3455465465467179","tahun_lahir_wali":"1970","kode_pendidikan":"01","kode_pekerjaan":"01","kode_penghasilan":"1"}`;
        const result = await Wali.postWali();
        expect(result.status).toEqual(200);
        //cek data yang diinputkan ke dalam database
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//edit wali
describe('PUT/wali/update/id', () =>{
    it('Membuat fungsi untuk mengubah data yang diinputkan dari tbl_wali dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        
        var obj=`{"kode_wali":"W004","nama_wali":"Test Update Wali","nik_wali":"3453425362000","tahun_lahir_wali":"1977","kode_pendidikan":"01","kode_pekerjaan":"01","kode_penghasilan":"1"}`;
        const result = await Wali.putWali();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek data yang diinputkan untuk edit ke db
        expect(result.config.data).toEqual(obj);
        //console.log(result.config);
    });
});

//hapus wali
describe('DELETE/wali/id', () =>{
    it('Membuat fungsi untuk menghapus data sesuai id yang dipilih dari tbl_wali  dan mengembalikan nilai dengan status 200 jika eksekusi sesuai', async () =>{
        const result = await Wali.deleteWali();
        //cek koneksi ke api
        expect(result.status).toEqual(200);
        //cek status hapus
        expect(result.statusText).toEqual('OK');
        //console.log(result);
    });
});