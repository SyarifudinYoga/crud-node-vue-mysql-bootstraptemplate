// import express
import express from "express";
// import connection
import db from "../config/database.js";
import isAuthorized from "../config/auth.js"
import jwt from "jsonwebtoken";


// import function from controller
import { showPendidikan, showPendidikanById, createPendidikan, updatePendidikan, deletePendidikan, searchPendidikanByName } from "../controllers/pendidikan.js";
import { createPekerjaan, deletePekerjaan, searchPekerjaanByName, showPekerjaan, showPekerjaanById, updatePekerjaan } from "../controllers/pekerjaan.js";
import { showAlamat, showAyah, showBeasiswa, showIbu, showKebutuhanKhusus, showKeluar, showKIP, showKPS, showPenghasilan, showPrestasi, showRegis, showUjian, showUsia, showWali} from "../controllers/foreign.js"
import { createSiswa, deleteSiswa, searchSiswaName, showSiswa, showSiswaById, updateSiswa } from "../controllers/siswa.js"
import { createAyah, deleteAyah, searchAyahByName, showAyahById, updateAyah } from "../controllers/ayah.js"
import { createIbu, deleteIbu, searchIbuByName, showIbuById, updateIbu } from "../controllers/ibu.js"
import { createWali, deleteWali, searchWaliByName, showWaliById, updateWali } from "../controllers/wali.js"
import { createRegister } from "../controllers/login.js";
// init express router
const router = express.Router();

//Tabel Login
router.post('/login/regis', createRegister);
router.post('/login', (req, res) => {
  //membuat end point untuk login akun
  var email = req.body.email; // mengimpor email dari form
  var password = req.body.password; //mengimpor password dari form
  const sql = "SELECT * FROM user WHERE email = ? AND password = ?"; // mencocokkan data form dengan data tabel
  if (email && password) {
    // jika email dan password ada
    db.query(sql, [email, password], function(err, rows) {
      if (err) throw err;
      // jika error akan menampilkan errornya
      else if (rows.length > 0) {
        // jika kolom lebih dari 0
          jwt.sign({ email, password },"SuperSecRetKey",{expiresIn: 60 * 60 * 7},(err, token) => {
              res.send(token); 
          })
        
      } else {
        res.json({
          message: "Email atau Password salah"
        }); // jika semua if tidak terpenuhi maka menampilkan kalimat tersebut
      }
    });
  }
});
router.put("/login/edit/:id",isAuthorized, function(req, res) {
  let data = // membuat variabel data yang berisi sintaks untuk mengupdate tabel di database
    'UPDATE user SET nama="' +req.body.nama +'", alamat="' +req.body.alamat +'", password="' +req.body.password +
    '", no_telp="' +req.body.telepon +'", email="' +req.body.email +'" where id=' +req.params.id;
  db.query(data, function(err, result) {
    // mengupdate data di database
    if (err) throw err;
    // jika gagal maka akan keluar error
    else {
      res.json({
        success: true,
        message: "Data berhasil diupdate"
      });
    }
  });
});
router.delete("/login/delete/:id",isAuthorized, function(req, res) {
  // membuat end point delete
  let id = "delete from user where id=" + req.params.id;

  db.query(id, function(err, result) {
    // mengupdate data di database
    if (err) throw err;
    // jika gagal maka akan keluar error
    else {
      res.json({
        success: true,
        message: "Data berhasil dihapus"
      });
    }
  });
});
//Tabel Ibu
router.get('/ibu/view', showIbu);
router.get('/ibu/view/:id', showIbuById);
router.get('/ibu/search/:id', searchIbuByName);
router.get('/ibu/search/', showIbu);
router.post('/ibu/add', createIbu);
router.put('/ibu/update/:id', updateIbu);
router.delete('/ibu/delete/:id', deleteIbu);
//Tabel Ayah
router.get('/ayah/view', showAyah);
router.get('/ayah/view/:id', showAyahById);
router.get('/ayah/search/:id', searchAyahByName);
router.get('/ayah/search/', showAyah);
router.post('/ayah/add', createAyah);
router.put('/ayah/update/:id', updateAyah);
router.delete('/ayah/delete/:id', deleteAyah);
//Tabel Wali
router.get('/wali/view', showWali);
router.get('/wali/view/:id', showWaliById);
router.get('/wali/search/:id', searchWaliByName);
router.get('/wali/search/', showWali);
router.post('/wali/add', createWali);
router.put('/wali/update/:id', updateWali);
router.delete('/wali/delete/:id', deleteWali);
//Tabel Pendidikan
router.get('/pendidikan/view', showPendidikan);
router.get('/pendidikan/view/:id', showPendidikanById);
router.get('/pendidikan/search/', showPendidikan);
router.get('/pendidikan/search/:id', searchPendidikanByName);
router.post('/pendidikan/add', createPendidikan);
router.put('/pendidikan/update/:id', updatePendidikan);
router.delete('/pendidikan/delete/:id', deletePendidikan);
//Tabel Pekerjaan
router.get('/pekerjaan/view', showPekerjaan);
router.get('/pekerjaan/view/:id', showPekerjaanById);
router.get('/pekerjaan/search/:id', searchPekerjaanByName);
router.get('/pekerjaan/search/', showPekerjaan);
router.post('/pekerjaan/add', createPekerjaan);
router.put('/pekerjaan/update/:id', updatePekerjaan);
router.delete('/pekerjaan/delete/:id', deletePekerjaan);
//Tabel Siswa
router.get('/siswa/view', showSiswa);
router.get('/siswa/view/:id', showSiswaById);
router.get('/siswa/search/:id', searchSiswaName);
router.get('/siswa/search/', showSiswa);
router.post('/siswa/add', createSiswa);
router.put('/siswa/update/:id', updateSiswa);
router.delete('/siswa/delete/:id', deleteSiswa);
//foreign
router.get('/alamat/view', showAlamat);
router.get('/kps/view', showKPS);
router.get('/kip/view', showKIP);
router.get('/ayah/view', showAyah);
router.get('/ibu/view', showIbu);
router.get('/wali/view', showWali);
router.get('/prestasi/view', showPrestasi);
router.get('/beasiswa/view', showBeasiswa);
router.get('/regis/view', showRegis);
router.get('/ujian/view', showUjian);
router.get('/keluar/view', showKeluar);
router.get('/penghasilan/view', showPenghasilan);
router.get('/kebutuhanKhusus/view', showKebutuhanKhusus);
router.get('/siswa/usia/', showUsia);

// export default router
export default router;