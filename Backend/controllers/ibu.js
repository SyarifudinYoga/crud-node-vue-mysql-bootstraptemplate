// Import function from Product Model

import { deleteIbuById, getIbu, getIbuById, insertIbu, searchIbu, updateIbuById } from "../models/ibuModel.js";

// Get All Products
export const showIbu = (req, res) => {
    getIbu((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const showIbuById = (req, res) => {
    getIbuById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Create New Product
export const createIbu = (req, res) => {
    const data = req.body;
    insertIbu(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Update Product
export const updateIbu = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updateIbuById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Delete Product
export const deleteIbu = (req, res) => {
    const id = req.params.id;
    deleteIbuById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const searchIbuByName = (req, res) => {
    searchIbu(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}