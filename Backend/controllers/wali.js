// Import function from Product Model
import { deleteWaliById, getWali, getWaliById, insertWali, searchWali, updateWaliById } from "../models/waliModel.js";

// Get All Products
export const showWali = (req, res) => {
    getWali((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const showWaliById = (req, res) => {
    getWaliById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Create New Product
export const createWali = (req, res) => {
    const data = req.body;
    insertWali(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Update Product
export const updateWali = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updateWaliById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Delete Product
export const deleteWali = (req, res) => {
    const id = req.params.id;
    deleteWaliById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const searchWaliByName = (req, res) => {
    searchWali(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}