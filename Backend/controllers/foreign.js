// Import function from Product Model
import { getAlamat, getAyah, getBeasiswa, getIbu, getKebutuhanKhusus, getKeluar, getKIP, getKPS, getPenghasilan, getPrestasi, getRegis, getUjian, getUsiaSiswa, getWali } from "../models/foreignModel.js";

// Get All Products
export const showAlamat = (req, res) => {
    getAlamat((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showKPS = (req, res) => {
    getKPS((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showKIP = (req, res) => {
    getKIP((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showAyah = (req, res) => {
    getAyah((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showIbu = (req, res) => {
    getIbu((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showWali = (req, res) => {
    getWali((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showPrestasi = (req, res) => {
    getPrestasi((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showBeasiswa = (req, res) => {
    getBeasiswa((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showRegis = (req, res) => {
    getRegis((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showUjian = (req, res) => {
    getUjian((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showKeluar = (req, res) => {
    getKeluar((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showPenghasilan = (req, res) => {
    getPenghasilan((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
// Get All Products
export const showKebutuhanKhusus = (req, res) => {
    getKebutuhanKhusus((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

export const showUsia = (req, res) => {
    getUsiaSiswa((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}