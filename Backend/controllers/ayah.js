// Import function from Product Model
import { deleteAyahById, getAyah, getAyahById, insertAyah, searchAyah, updateAyahById } from "../models/ayahModel.js";

// Get All Products
export const showAyah = (req, res) => {
    getAyah((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const showAyahById = (req, res) => {
    getAyahById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Create New Product
export const createAyah = (req, res) => {
    const data = req.body;
    insertAyah(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Update Product
export const updateAyah = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updateAyahById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Delete Product
export const deleteAyah = (req, res) => {
    const id = req.params.id;
    deleteAyahById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const searchAyahByName = (req, res) => {
    searchAyah(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}