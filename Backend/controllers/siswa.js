// Import function from Product Model
import { deleteSiswaById, getSiswa, getSiswaById, insertSiswa, searchSiswa, updateSiswaById } from "../models/siswaModel.js";

// Get All Products
export const showSiswa = (req, res) => {
    getSiswa((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const showSiswaById = (req, res) => {
    getSiswaById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Create New Product
export const createSiswa = (req, res) => {
    const data = req.body;
    insertSiswa(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Update Product
export const updateSiswa = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updateSiswaById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Delete Product
export const deleteSiswa = (req, res) => {
    const id = req.params.id;
    deleteSiswaById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const searchSiswaName = (req, res) => {
    searchSiswa(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}