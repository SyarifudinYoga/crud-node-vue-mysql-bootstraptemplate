// Import function from Product Model
import { deletePekerjaanById, getPekerjaan, getPekerjaanById, insertPekerjaan, searchPekerjaan, updatePekerjaanById } from "../models/pekerjaanModel.js";

// Get All Products
export const showPekerjaan = (req, res) => {
    getPekerjaan((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const showPekerjaanById = (req, res) => {
    getPekerjaanById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Create New Product
export const createPekerjaan = (req, res) => {
    const data = req.body;
    insertPekerjaan(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Update Product
export const updatePekerjaan = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updatePekerjaanById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Delete Product
export const deletePekerjaan = (req, res) => {
    const id = req.params.id;
    deletePekerjaanById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const searchPekerjaanByName = (req, res) => {
    searchPekerjaan(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}